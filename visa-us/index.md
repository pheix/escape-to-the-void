# Получение US визы в Бишкеке

## Подготовительные шаги

Запрос на собеседование в консульстве США в Бишкеке: https://gitlab.com/pheix/escape-to-the-void/-/raw/main/visa-us/assets/appointment-request.12.10.2022.pdf

Ответ: https://gitlab.com/pheix/escape-to-the-void/-/raw/main/visa-us/assets/appointment-response.10.11.2022.pdfм

> Благодарим вас за ваше недавнее электронное письмо с просьбой записать на прием для получения неиммиграционной визы в посольстве США в Бишкеке. К сожалению, в настоящее время у нас нет свободных мест, которые мы могли бы предложить. Новые слоты станут доступны после нового года. Если вы по-прежнему заинтересованы в том, чтобы записаться на прием после нового года в Бишкеке, отправьте нам последующее электронное письмо с темой "Non-citizen/non-resident appointment request" и информацией, приведенной в таблице ниже. Можно запросить семейные (групповые) записи.
>
>1. Имя и фамилия заявителя:
>1. Дата рождения:
>1. Тип визы:
>1. Планируемая дата поездки:
>1. Имя контактного лица/организации или приглашающей стороны в США:
>1. Адрес электронной почты и номер телефона заявителя:
>1. Номер штрих-кода (начинается с AA):
>1. Цель поездки и причина запроса ускоренной записи:
>
> Вы также можете подать заявление на получение визы в других консульских отделах. Время ожидания записи доступно здесь: https://travel.state.gov/content/travel/en/us-visas/visa-information-resources/wait-times.html

## План действий

> * Имя контактного лица/организации или приглашающей стороны в США: https://www.perlfoundation.org/
> * Цель поездки и причина запроса ускоренной записи: обсуждени результатов Perl Conference, personal meeting with со steering councils of Perl foundation, panel discussion about Central Asia Perl Conference.

1. Написать сопроводительное письмо, references: [запрос 10.2022](https://gitlab.com/pheix/escape-to-the-void/-/raw/main/visa-us/assets/appointment-request.12.10.2022.pdf), [inviatation letter canada](https://gitlab.com/pheix/escape-to-the-void/-/tree/main/visa-canada#invitation-letter);
2. Отправить электронное письмо с темой **"Non-citizen/non-resident appointment request"** и информацией, приведенной в таблице выше

Таким образом нужно сначала закрыть секцию invitation letter для Канады, а затем написать сопроводительное письмо для US. В письме важно изложить историю с продлением визы в Австрии (приложить email переписку, как это было сделано для шенгена).

**{-&nbsp;WIP&nbsp;-}**
