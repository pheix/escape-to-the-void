# Escape to the void

[![](http://img.youtube.com/vi/o3iozdMCT5w/0.jpg)](https://youtu.be/o3iozdMCT5w "Sepultura — Escape to the void")

---

> 17.05.2024

## Акутальные планы

1. {-&nbsp;31.05&nbsp;-} Подача заявления на [StartUP Visa Portugal](https://gitlab.com/pheix/escape-to-the-void/-/tree/main/visa-startup/portugal)

## ⚠️ Старые планы (totally deprecated)

### Двухнедельный план

1. {+&nbsp;27.09&nbsp;+} забронировать отель
2. {+&nbsp;30.09&nbsp;+} выезд в Бишкек (пятница)
3. {+&nbsp;Прибытие в 15:15 по местному времени&nbsp;+}:
    * ✅ сим карта;
    * ✅ заезд в отель.
4. {+&nbsp;Поиск квартиры&nbsp;+}: лесная цитадель
5. {+&nbsp;02.10&nbsp;+} Поиск коворкинга
6. {+&nbsp;03.10&nbsp;+} Талон о временной регистрации
8. {+&nbsp;04.10&nbsp;+} Оформление карт банков
    * Capital bank (Элкарт) - https://www.capitalbank.kg/
    * Bakai Bank (Элкарт, VISA gold) - https://www.bakai.kg/ru/
    * Demir bank (VISA classic) - https://demirbank.kg/ru
9. {+&nbsp;04.10&nbsp;+} Новая квартира: заезд в лесную цитадель
10. {+&nbsp;08.10&nbsp;+} Получение карты (Bakai, Demir), [выставление реквизитов](https://bit.ly/3CltP6g)
11. {+&nbsp;09.10&nbsp;+} [Решение](https://gitlab.com/pheix/escape-to-the-void/-/issues/1) о переeзде на новую квартиру (уже 3-ю) — ✅ положительное решение принято, переезжаю 11.10
12. {+&nbsp;09.10&nbsp;+} Оформить телефонный номер на себя в MegaCom, [офисы](https://www.megacom.kg/pages/tsentry-prodazh-i-obsluzhivaniya?locale=ru) — ✅ сделал в Бишкек-парке
13. {+&nbsp;11.10&nbsp;+} Получение карты Capital bank
14. {+&nbsp;11.10&nbsp;+} Переезд на Московскую 91
15. {+&nbsp;11.10&nbsp;+} Получение денег на счет Bakai Bank
17. {+&nbsp;14.10&nbsp;+} [Поход](https://gitlab.com/pheix/escape-to-the-void/-/tree/main/visa-schengen) в VFS global: [ул. Ибрагимова 103](https://go.2gis.com/4aa1l), Бизнес Центр Виктория, 11 этаж — прощло позитивно, тезисы: заявку на визу от нерезидента примут, дополнительно нужно сделать мотивационное письмо и справку о ниличии счета в киргизском банке, добавил требования [сюда](https://gitlab.com/pheix/escape-to-the-void/-/tree/main/visa-schengen);

### Долгорочный план

1. {+&nbsp;18.10.2022&nbsp;+} Оплата консульского взноса для записи на себеседование для получения визы США;
2. {+&nbsp;04.11.2022&nbsp;+} Оформление доставки паспорта из Вены в [FedEx/TNT](https://www.interpost.kg/): https://go.2gis.com/92pmw:
    * delivery service: TNT Express
    * booking identifier: FRU 901XXX
    * destination: Bishkek, Kyrgyzstan
    * delivery to: TNT/FedEx office in Bishkek: ul. Chingiz Ajtmatov, 38
    * delivery for: Narkhov Konstantin, +996XXXXXX804, +7916XXXXXX59
    * заказ вылетел из Вены `7 Nov 2022 22:30 - Wien-Flughafen, Shipment in transit` — ссылка на отслеживание в почте, ожидаемая дата доставки 10.11.2022;
    * **паспорт доставлен 11.11.2022, забрал его 12.11 — он был в Австрии более 2х лет!**
3. {+&nbsp;08.11.2022&nbsp;+} Первый этап подготовки документов:
    * Чек-лист документов для шенгенской визы;
    * Мотивационное письмо для шенгенской визы.
4. {+&nbsp;09.01.2023&nbsp;+} Получение выписки в Demir Bank;
5. {+&nbsp;26.02&nbsp;+} Получение выписки в Bakai Bank;
6. {+&nbsp;21.03&nbsp;+} Второй этап подготовки документов на шенгенскую визу: документы из чек-листа, доступные на данный момент;
7. {+&nbsp;26.03&nbsp;+} Третий этап подготовки документов на шенгенскую визу: покупка билетов, бронирование отелей, запись на подачу документов в VFS global в Бишкеке;
6. {+&nbsp;04.04&nbsp;+} [Подача документов на Шенгенскую визу](https://gitlab.com/pheix/escape-to-the-void/-/tree/main/visa-schengen) в VFS global.
7. {+&nbsp;05.05&nbsp;+} [Подача документов на визу Канады](https://gitlab.com/pheix/escape-to-the-void/-/tree/main/visa-canada/) в электронном виде.

## ⚠️ Зависшие и невыполненные дела

1. ~Запись на собеседование для получения визы США: по состоянию на 23.10 нет окрытых дат, веду постоянный мониторинг в т.ч. через профильную тему на [форуме Винского](https://forum.awd.ru/viewtopic.php?f=326&t=341111&sid=01535af69fc56a5c164bd06fa8c83047&start=1820)~ → записался 4.11;

### Заметки из невыполненного

> 1. Согласовать с консульством США в Австрии возможность выдачи визы в паспорт, который не указан в DS-160 — т.е. я приезжаю, забираю паспорт, прохожу собеседование и, если оно завершается успехом, отдаю только что забранный паспорт на вклейку визы, таким образом там у меня будет две визы США в новом паспорте;
> 2. Определиться с датой собеседования на визу США — планирую на декабрь, выезд из квартиры будет 11.12, так что можно, например, улететь из Бишкека 11.12, а 12 декабря уже пройти собеседование.

~По-видимому, эти пункты не имеют смысла при записи на дальнюю дату (записан на 05.05.2023). Поэтому выглядит логичным план забрать паспорт из Вены с помощью FedEx/TNT.~ Паспорт доставлен `11.11.2022`, забрал его `12.11` — он был в Австрии более 2х лет!

## Переезд вещи

https://gitlab.com/pheix/escape-to-the-void/-/tree/main/urgent

В итоге забыл свою термокружку в машине по пути в аэропорт.

## License information

You can redistribute the content of this repository under the terms of the [CC BY-NC-ND 3.0](https://creativecommons.org/licenses/by-nc-nd/3.0/) license (информация о лицензии и использовании этого репозитория на русском языке: https://creativecommons.org/licenses/by-nc-nd/3.0/deed.ru).

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
