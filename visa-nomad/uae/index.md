# Nomad visa UAE

Virtial Work residency permit: https://gdrfad.gov.ae/en/services/64154a31-ec6d-11ec-140b-0050569629e8

Дополнительная информация: https://www.visitdubai.com/en/invest-in-dubai/live-and-work/visas-and-entry/work-remotely-from-dubai

## Traction

Список документов:

1. Действующий контракт;
2. Письмо от работодателя;
3. Выписка из банка.

Пункты `1` and `2` в работе, **27.06** в планах организовать доставку. Выписка из банка планирую сделать **1.07**.
