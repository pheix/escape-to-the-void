# Получение канадской визы в Бишкеке

Рассчет времени обработки для заявлений, поданных из Кыргызстана — {-&nbsp;354 days&nbsp;-} 😱

https://www.canada.ca/en/immigration-refugees-citizenship/services/application/check-processing-times.html

## 21.11

Application approved:

![Application approved screenshot](https://gitlab.com/pheix/escape-to-the-void/-/raw/main/visa-canada/assets/img/application-approved.png?inline=true)

## 28.06

Подготовка и отправка запроса по срокам рассмотрения: https://gitlab.com/pheix/escape-to-the-void/-/issues/6

## 18.05

**Biometrics** — May 18, 2023 {+&nbsp;Completed&nbsp;+}.

## 17.05

Applied on `May 17, 2023 12:33 AM`.

Стоимость **185 CAD** (~$138), оплатил картой Бакай банка:

```
Amount: $185.00
**** **** **** 1247
Transaction Type : Purchase
Authorization Code: 442***
Reference Number: 663*********744340
```

Получил письмо (сообщение) с приглашением явиться для сдачи отпечатков. Пришел в VFS Canada, там сказали, что нужно предварительно записаться (действительно, так и было указано в письме). Создал личный кабинет прямо с телефона и забронировал визит на 18.05 на 9:00.

## Checklist for submission

1. Print:
    * ✅ imm5707e
    * ~imm5257e~ — не требует подписи (https://www.canadavisa.com/canada-immigration-discussion-board/threads/how-to-sign-imm5257e-for-online-application.675349/)
    * ✅ imm5257b_1
    * ~invitation letter~
    * ~employment letter~
    * ✅ cover letter
2. Sign
    * ✅ imm5707e
    * ~imm5257e~
    * ✅ imm5257b_1
    * ✅ cover letter
3. Scan
    * ✅ imm5707e (signed)
    * ✅ imm5257b_1 (signed)
    * ✅ cover letter (signed)
    * ✅ Текущий загранпаспорт
    * ✅ US виза
    * ✅ Талон о регистрации в КР и его перевод, заверенный у местного нотариуса
    * ✅ Банковская справка и выписка из Бакая
    * ✅ платежка о внесении средств

## Invitation letter

> You must provide a letter of invitation from the person inviting you to Canada. The letter must be written by the host and should have specific information about the host and invitee (https://onlineservices-servicesenligne.cic.gc.ca/eapp/exit?logout=true&uri=https://www.cic.gc.ca/english/visit/letter.asp).
>
> The letter should state the purpose and length of the visit, the nature of the relationship between you and the host, the contact information of the host, etc.
>
> If you are being invited to conduct business in Canada, a letter of invitation should be printed on the company's letterhead and include:
>
> * the host's full name, title and business contact information
> * a brief summary of the reason for the invitation, including details of the business or trade to be undertaken
> * the full names of all employees from your company who are being invited by your host
> * the intended duration and a detailed itinerary of the visit
> * a statement specifying who will be responsible for all travel-related expenses

Мне требуется:

1. ✅ Подготовить текст просьбы к организаторам PerlConf2023 написать пригласительное письмо;
2. Простой шаблон: https://www.letters.org/invitation-letter/conference-invitation-letter.html, но в нем нудно учесть требования для визы Канады;
3. ✅ Отправить его (отправлено 01.05.2023):
    * The Perl and Raku Conference 2023: https://tprc.to/tprc-2023-tor/contact-us/
    * The Perl Foundation: hello@perlfoundation.org

### ✅ Invitation letter request

Good afternoon!

Here's Konstantin Narkhov. How are you? Hope everything goes well. My talks [1](https://www.papercall.io/talks/210085/children/242453) and [2](https://www.papercall.io/talks/242251/children/242452) were accepted for Toronto Perl Conference 2023. Now I'm preparing for visa application at Canadian consulate. Canada requires invitation letter for applicants. Could you issue it for me?

I visited the consulate center on Wednesday and collected general requirements:

1. the letter must be written by the host and should have specific information about the host and invitee;
2. the letter should state the purpose and length of the visit, the nature of the relationship between you and the host, the contact information of the host, etc.

I found a few templates on web, this one looks good: https://www.letters.org/invitation-letter/conference-invitation-letter.html.

My personal details to be mentioned in the letter:

1. *First/last name:* Konstantin Narkhov;
2. *Email*: konstantin@narkhov.pro;
3. *Phone*: +996XXXXXX804;
4. *Residence address:* ~720040~ **720001**, Kyrgyz Republic, Bishkek city, Togolok Moldo 3A-19;
5. *Visit lenght*: 7 days — 10 July, 2023 - 17 July, 2023;
6. ~*Relationship between you and the host*~ → [optional documents](https://gitlab.com/pheix/escape-to-the-void/-/tree/main/visa-canada#optional-documents).

I would like you to send me a signed letter scan in PDF. Thanks in advance! Ping me on any questions or suggestions. Also it's a bit "hot" situation — just 2,4 months till the conf, so I will be very happy if you will issue a letter asap.

Sincerely yours, Konstantin Narkhov.

> ### Draft
>
> [Proposed Invitation letter](https://gitlab.com/pheix/escape-to-the-void/-/raw/main/visa-canada/assets/pdf/invitation-letter-draft.pdf).
>
> **{-&nbsp;Замечания&nbsp;-}**
>
> 1. Добавить контактные телефоны (для ускорения возможной связи со стороны сотрудника консульства);
> 2. Поменять Индекс — должен быть **720001**;
> 3. *Relationship between you and the host* — к сожалению нет никакой информации, предлагается внести ее в мотивационное письмо: https://gitlab.com/pheix/escape-to-the-void/-/tree/main/visa-canada#optional-documents

## Заполнение анкеты и форм

### Application Form(s)

🖨️ APPLICATION FOR VISITOR VISA (TEMPORARY RESIDENT VISA), [[ref](https://www.canada.ca/content/dam/ircc/migration/ircc/english/pdf/kits/forms/imm5257e.pdf)].

### Supporting Documents

1. Family Information Form (IMM5707), [[ref](https://www.canada.ca/content/dam/ircc/migration/ircc/english/pdf/kits/forms/imm5707e.pdf)];
2. Travel History;
3. Passport — you must submit a legible copy of your valid travel document which you will use to travel (the page that shows your birth date and country of origin, and any pages with stamps, visas or markings);
4. {+&nbsp;Invitation Letter&nbsp;+};
5. Proof of Means of Financial Support;
6. Digital photo;

#### Travel History

Вот такой список документов будет здесь:

1. {+&nbsp;Скан US визы&nbsp;+};
2. ~Скан всех страниц текущего загранпаспорта~ — полный скан (все страницы) действующего паспорта загружается в раздел **Supporting Documents**;
2. {+&nbsp;Скан всех страниц старого загранпаспорта&nbsp;+};
3. {+&nbsp;Скан талона о регистрации в КР и его перевод, заверенный у местного нотариуса&nbsp;+}.

> You must provide information on your valid visa from the United States as well as previous travel history. This can include copies of
>
> * {+&nbsp;your previous passports and/or visas (used within the last 10 years to travel outside your country)&nbsp;+};
> * entry and exit stamps
> * study and/or work permits that indicate your duration outside of your country
> * expired or valid visas
> * boarding passes
>
> If you hold a valid visa from the United States, provide a clear, scanned copy of your visa. If you are living outside your country of citizenship, please submit a copy of your immigration status document for the country where you currently reside. This could be a work permit, study permit, visa, or any other document that authorizes you to be in the country where you are living. ~Полагаю нужно сделать перевод талона о регистрации и заверить его у местного нотариуса~ — {+&nbsp;Готово!&nbsp;+}

#### Proof of Means of Financial Support

If you are visiting Canada, you must prove that you can support yourself and the family members who will be with you while you are in Canada. Provide as many of these documents as you can:

* your bank statements for the past four months ({+&nbsp;выписка с альфа счета от 17.04 в reg-почте, апрельская справку о состоянии счета&nbsp;+});
* pay stubs — платежные квитанции
* {+&nbsp;an employment letter&nbsp;+}, [#2]
* proof of assets or ~business~ (свидетельства на квартиры)

> * tax reports, declarations or statements
> * a bank draft in convertible currency
> * proof of payment of tuition and accommodation fees
> * proof of a student/education loan from a financial institution
> * a letter from the person or institution providing you with money
> * proof of funding paid from within Canada, if you have a scholarship or are in a Canadian-funded educational program
> * proof of a Canadian bank account in your name if money has been transferred to Canada

### Optional documents

1. Schedule 1 - Application for a Temporary Resident Visa Made Outside Canada (IMM 5257), [[ref](https://www.canada.ca/content/dam/ircc/migration/ircc/english/pdf/kits/forms/imm5257b_1.pdf)];
2. {+&nbsp;Client Information&nbsp;+} [#3]
