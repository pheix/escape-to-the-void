# Immigrate with a start-up visa

Details: https://www.canada.ca/en/immigration-refugees-citizenship/services/immigrate-canada/start-visa/eligibility.html

Business incubators review: https://gitlab.com/pheix/escape-to-the-void/-/tree/main/visa-canada/startup/business-incubators

## Сортировка инкубаторов

### Бесплатные

1. **York Entrepreneurship Development Institute**, SUV: https://yedi.ca/programs/ventures-in-residence/start-up-visa/, 12 months, **{+&nbsp;free&nbsp;+}**, application portal will reopen on May 13, 2024.

### До $20,000 CAD

1. **Planet Hatch**, SUV: https://www.planethatch.com/start-up-visa, 6 months membership, {+$3,180+} CAD for Letter of Support stage;
2. **Innovation Cluster - Peterborough and the Kawarthas**, SUV: https://innovationcluster.ca/programs/startup-visa-canada/, 3 months for virtual programs and 12 months for in-office program, {+$16,000+} CAD;
3. **Global Startups Accelerator**, SUV: https://www.globalstartups.tech/startup-visa-program, 6 months, {+$18,000+} CAD;
4. **VIATEC**, SUV: https://www.viatec.ca/venture-acceleration-program/, 12 months, {+$1,500+} CAD per month: {+$18,000+} CAD total, открытые вопросы: $1.5k в месяц с каждого фаундера или с команды, также помесячная оплата выгодна в смысле расходов. Также на странице указано, что оплата выполняется после получения Letter of Support и релокации, это означает, что для оплаты программы можно использовать предварительно задекларированные для визы $13,000 CAD;
5. **Alberta IoT Association**, SUV: https://programs.albertaiot.com/start-up-visa/, 6 month program, no equity in your company, {+$20,000+} CAD — `starter` plan.

### По продолжительности программы

1. **Roseview Global Incubator**, up to 30 months, SUV: https://roseviewglobal.com/suv-program/, 2 iterations: [BASE](https://roseviewglobal.com/base-program/) and [RISE](https://roseviewglobal.com/rise-incubator-program/) programs, [fees](https://roseviewglobal.com/fees/): {-$28,000-} CAD;
2. **Launch Academy - Vancouver**, 12 months (up to 24 months), SUV: https://www.launchacademy.ca/maple-program-details/, {-$42,000-} CAD;
3. **North Forge Technology Exchange**, 18 months, SUV: https://northforge.ca/programs/suv/, SUV FAQ: https://northforge.ca//programs/suv/suv-faqs/, there is no cost to apply to the Startup Visa Program; the cost of the Founders Program varies depending on number of founders. Please contact suv@northforge.ca to inquire further;

### Программы на 12 месяцев

1. **Alacrity Foundation**, SUV: https://www.alacritycanada.com/startup-visa-program/, 12 month program, no cost defined;
2. **Brilliant Catalyst**, SUV: https://www.ontariotechbrilliant.ca/programs/suv, 12 month program, no cost is defined;
3. **Platform Calgary**, SUV: https://www.platformcalgary.com/program/global-startup-visa, 1 year, {-$30,000-} CAD;
4. **YSpace (York University)**, SUV: https://www.yorku.ca/yspace/programs-streams/startup-visa/, two stages: an exclusive 6-week premium training and mentorship experience + a year-long bespoke coaching and mentorship experience (launchpad), the program cost will be {-$30,000-} CAD total ({-$10,000-} CAD for Blueprint and {-$20,000-} CAD for Launchpad).

### Заинтересовали (интуиция)

1. **Alberta IoT Association**, SUV: https://programs.albertaiot.com/start-up-visa/;
2. **Manitoba Technology Accelerator**, SUV: https://mbtechaccelerator.com/international-entrepreneurs/, no program duration, costs $2,100 CAD per month, fast review;
3. **LaunchPad PEI**, SUV: https://launchpadpei.com/start-up-visa/.

#### LaunchPad PEI

Information about Prince Edward Island: https://www.princeedwardisland.ca/en/topic/pei-economy

Economy report 2022: https://www.princeedwardisland.ca/sites/default/files/publications/web_asr.pdf (must have one for market analysis and project targeting).

## Анализ

### Финансовая шпаргалка

|Назначение | CAD | USD | EUR |
|:----------|----:|----:|----:|
| Сумма на банковском счету для IRCC | 13,000 | 9,900 | 8,900 |
| Launch Academy - Vancouver | 42,000 | 31,800 | 28,700 |
| YSpace (York University) / Platform Calgary | 30,000 | 22,700 | 20,500 |
| Roseview Global Incubator | 28,000 | 21,200 | 19,200 |
| Alberta IoT Association | 20,000 | 15,200 | 13,700 |
| Viatec / Global Startups Accelerator | 18,000 | 13,600 | 12,300 |
| Innovation Cluster - Peterborough and the Kawarthas | 16,000 | 12,100 | 11,000 |
| YSpace Blueprint program | 10,000 | 7,600 | 6,850 |
| Planet Hatch | 3,180 | 2,500 | 2,200 |
| Manitoba Technology Accelerator (monthly) | 2,100 | 1,600 | 1,450 |
| Viatec (monthly) | 1,500 | 1,150 | 1,050 |

**{-&nbsp;WIP&nbsp;-}**
