# Business incubators

## Full review for business incubators

List of official [business incubators](https://www.canada.ca/en/immigration-refugees-citizenship/services/immigrate-canada/start-visa/designated-organizations.html#incubators):

1. **Alacrity Foundation**, SUV: https://www.alacritycanada.com/startup-visa-program/, 12 month program, no cost defined;
2. **Alberta IoT Association**, SUV: https://programs.albertaiot.com/start-up-visa/, 6 month program, no equity in your company, {-$20,000-} CAD — `starter` plan and {-$30,000-} — `complete` plan. Also they want a *company with a minimum of 2 years of proven revenue and work in the IoT and/or Emerging Tech industry*;
3. **Altitude Accelerator**, programs: https://altitudeaccelerator.ca/programs-1/, but no details about SUV and program duration. One interesting item: *RevUp* — test your new business idea on your own time, at your own pace, while having access to Altitude’s team and resources;
4. **BHive**, SUV: https://thebhive.ca/startup-visa-program/, 6 month program, no cost defined;
5. **Manitoba Technology Accelerator**, SUV: https://mbtechaccelerator.com/international-entrepreneurs/, no program duration, costs $2,100 CAD per month, fast review: *we use a prestigious third-party screening committee to review applications within 21 business days. They provide recommendations or suggestions within 48 hours*;
6. **Brilliant Catalyst**, SUV: https://www.ontariotechbrilliant.ca/programs/suv, 12 month program, no cost is defined;
7. **DNZ**, SUV: https://dmzventures.com/startup-visa-program/, 5 month program, {-$20,000-} CAD — 1st phase,  {-$30,000-} CAD — 2nd phase;
8. **Empowered Startups Ltd.**, SUV: https://empoweredstartups.com/canada-startup-visa/, no cost and duration are defined. Also looks like they works with Portugal SUV a well;
9. **Foresight Cleantech Accelerator Centre**, SUV: https://foresightcac.com/start-up-visa-program, no cost and duration are defined, but looks like a there is a program for 2 month ($100 CAD per month), they focused on [Net-Zero](https://www.canada.ca/en/services/environment/weather/climatechange/climate-plan/net-zero-emissions-2050.html);
10. **Innovation Cluster - Peterborough and the Kawarthas**, SUV: https://innovationcluster.ca/programs/startup-visa-canada/, 3 months for virtual programs and 12 months for in-office program. {+$16,000+} CAD per company. SUV runs three times a year, in February, June, and October: Apply for FEBRUARY 2024 by November 30th, *меня эта программа заинтересовала*;
11. **Innovation Factory / Runway Accelerator**, SUV: https://runway.innovationfactory.ca/program/, 6 months, {-$25,000-} CAD;
12. **Innovate Niagara**, SUV: https://www.innovateniagara.com/site/blog/2021/01/14/exciting-news-for-foreign-entrepreneurs, no cost and duration are defined, they want startups with a minimum of $CAD150,000 in revenue in the past 2 years
13. **Intrinsic Innovations**, SUV: https://www.intrinsicinnovations.ca/programs, no cost and duration are defined;
14. **ISM Arts & Culture Ltd**, SUV: https://www.ism-ac.com/incubator, 8-week program, tuition fees for successful applicants are determined based on the stage of the start-up;
15. **Planet Hatch**, SUV: https://www.planethatch.com/start-up-visa, 6 months, The total fee you pay depends on how far you go in the application process. At the end of stage 2 which is approval of your application, you are required to $1,000 + HST to move into the due diligence phase. If you are approved by the due diligence committee, you are further required to pay $2,180 + HST in order to receive a Letter of Support. This amount also includes the cost of your membership with Planet Hatch for 6 months upon arrival. Total {+$2,180+} CAD at Letter of Support stage, *меня эта программа заинтересовала*;
16. **Global Startups Accelerator**, SUV: https://www.globalstartups.tech/startup-visa-program, 6 months, {+$18,000+} CAD. Cohorts per year: January and June. Program fee is per company covering up to five co-founders. If you have questions about the application process, please send a message to Meg@GlobalStartups.tech
17. **Launch Academy - Vancouver**, SUV: https://www.launchacademy.ca/maple-program-details/, 12 months (up to 24 months), {-$42,000-} CAD, *по ощущениям эта программа подходит в большей степени для уже рабочих бизнесов, которые нужно релоцировать в Канаду, тем не менее описание программы меня заинтересовало*;
18. **LaunchPad PEI**, SUV: https://launchpadpei.com/start-up-visa/, no cost and duration are defined, application form: https://launchpadpei.blitzen.com/form/Startup-Visa-Program-LaunchPad-PEI-Application-Form, *я заинтересован!*;
19. **Niagara Business & Innovation Fund**, SUV: https://niagarafund.com/start-up-visa/, 22 weeks, no cost is defined, but *fees depend on the number of participants per company and other factors. Once the applicant meets the basic eligibility criteria for the program and the needs assessed, we provide a contractual agreement, which includes our fees*;
20. **North Forge Technology Exchange**, SUV: https://northforge.ca/programs/suv/, SUV FAQ: https://northforge.ca//programs/suv/suv-faqs/, there is no cost to apply to the Startup Visa Program, *after the Letter of Support is provided, startups will join the North Forge Founders Program for 18 months which provides the applicants with all the knowledge and support they need to launch and run their business in Canada*, the cost of the Founders Program varies depending on number of founders. Please contact suv@northforge.ca to inquire further, *описание программы меня заинтересовало, хороший FAQ*;
    * **North Forge East Ltd.**, SUV: https://www.northforgeeast.ca/service-for-entrepreneurs/international-startup-program, 6 months, no cost is defined;
21. **Pacific Technology Ventures**, SUV application: https://www.pactech.ca/apply, no cost and duration are defined;
22. **Platform Calgary**, SUV: https://www.platformcalgary.com/program/global-startup-visa, 1 year, {-$30,000-} CAD;
23. **Pycap**, SUV: https://www.pycap.com/, Price: {-$40,000-} CAD + applicable taxes; program duration: 6 month; Virtual or Office;
24. **Roseview Global Incubator**, SUV: https://roseviewglobal.com/suv-program/, 2 iterations: [BASE](https://roseviewglobal.com/base-program/) and [RISE](https://roseviewglobal.com/rise-incubator-program/) programs up to 30 months, [fees](https://roseviewglobal.com/fees/): {-$28,000-} CAD;
25. **Spark Commercialization and Innovation Centre**, SUV: https://sparkcentre.org/services/start-up-visa/, no cost and duration are defined, but there is the [Business Plan template](https://sparkcentre.org/wp-content/uploads/2023/07/Business-Plan-FEB-2021-FINAL-RF-V74.pdf);
26. **Spring Activator**, SUV: https://spring.is/programs/programs-impact-startup-visa-program-canada/, 3 months (???), {-$30,000-} CAD;
27. **Think8 Global Institute**, SUV: https://think8.ca/, no cost and duration are defined, *сложная анкета: https://think8.ca/Think8_Canada_SUV.pdf, однако интересный сайт и довольно прозрачное описание процесса*;
28. **TiE Toronto**, SUV: https://toronto.tie.org/startup/, 6 months, {-$25,000-} CAD;
29. **Treefrog**, SUV: https://www.treefrog.biz/international-program/, fast-paced 6-week program, {-$25,000-} CAD;
30. **ventureLAB Innovation Centre**, SUV: https://www.venturelab.ca/canada-catalyst#Start-up-Visa, 6 months, {-$30,000-} CAD;
31. **VIATEC**, SUV: https://www.viatec.ca/venture-acceleration-program/, 12 months, {+$1,500+} CAD per month: {+$18,000+} CAD total;
32. **Waterloo Accelerator Centre**, SUV: https://www.acceleratorcentre.com/global-frontier, no cost and duration are defined, applicants are required to have $100k CAD in annual revenue to participate;
33. **York Entrepreneurship Development Institute**, SUV: https://yedi.ca/programs/ventures-in-residence/start-up-visa/, 12 months, **{+&nbsp;free&nbsp;+}**, application portal will reopen on March 11, 2024, *меня эта программа заинтересовала*;
34. **YSpace (York University)**, SUV: https://www.yorku.ca/yspace/programs-streams/startup-visa/, two stages: an exclusive 6-week premium training and mentorship experience + a year-long bespoke coaching and mentorship experience (launchpad), the program cost will be {-$30,000-} CAD total ({-$10,000-} CAD for Blueprint and {-$20,000-} CAD for Launchpad), consider - we do not take any equity in the company!

## Skipped

1. **ALBERTA.CA**, SUV: https://www.alberta.ca/agrivalue-processing-business-incubator#jumplinks-6, The Food Processing Development Centre (FPDC) and APBI engage with clients and tenants in the development of food products, food manufacturing processes, pilot plant development, interim processing and food processing business incubation. In addition, the facilities are considered a 'designated incubator' under the SUV program with the Government of Canada. We may choose to certify those for SUV consideration provided they have already qualified for one of the programs offered at the FPDC and the APBI. We do not consider ourselves as a means to get a SUV. Rather, we utilize our status as a designated incubator to assist our clients and tenants with projects related to food manufacturing. For more information, email: af.startupvisa@gov.ab.ca.
    * [Agrivalue Processing Business Incubator](https://www.alberta.ca/agrivalue-processing-business-Incubator.aspx);
    * [Food Processing Development Centre](https://www.alberta.ca/food-processing-development-centre.aspx);
2. [Creative Destruction Lab](https://creativedestructionlab.com/program/) — No SUV program;
3. [Extreme Innovations](https://sites.google.com/view/extremeinnovations/home) — site looks as out of maintenance;
4. [Genesis Centre](https://www.genesiscentre.ca/start-up-visa) — We will no longer accept applications for the Startup Visa program from applicants outside of Canada. We encourage those living in Canada on temporary immigration status to apply if they meet the eligibility criteria outlined below.
5. [Highline BETA Inc.](http://www.highlinebeta.com/) — selecting startups [only from own incubator](https://highlinebeta.com/contact-us-startups/);
6. [Invest Nova Scotia](https://investnovascotia.ca/start-up/) — No SUV program;
7. [Innovate Calgary](https://innovatecalgary.com/) — No SUV program;
8. [Invest Ottawa](https://www.investottawa.ca) — No SUV program, but a lot of things are there;
9. [Millworks Centre for Entrepreneurship](https://themillworks.ca/en/programs-services/start-up-visa/) — We are not currently accepting applications to our Start-up Visa program. Please review the above eligibility requirements, and check back soon for applications to open;
10. [NEXT Canada](https://www.nextcanada.com/programs-overview/) — No SUV program;
11. [Real Investment Fund III L.P. o/a FounderFuel](https://founderfuel.com/apply/) — No SUV program;
12. [Red Leaf Capital Corp](https://www.redleafcapital.ca/accelerator-application) — No SUV program;
13. [The DMZ at Ryerson University](https://dmz.torontomu.ca/our-programs/) — No SUV program;
14. [Toronto Business Development Centre (TBDC)](https://www.tbdc.com/) — No SUV program;
15. [University of Toronto Entrepreneurship Hatchery](https://hatchery.engineering.utoronto.ca/) — No SUV program;
