# StartUP Visa Portugal

## Checklist

> 2024-05-18

### Entrepeneur

1. {+&nbsp;Add Qualification Degree certificate&nbsp;+}
    * ✅ scan
2. {-&nbsp;Add certificate or criminal record from the judicial services of the country of origin&nbsp;-}
    * get from authority
    * scan
3. {+&nbsp;Add bank declaration which demonstrates the existence of own financial resources and subsistence means equivalent to 12 times the International Accounting Standard (IAS) and attests the possibility of transferring those funds to a Bank institution in Portugal.&nbsp;+}
    * ✅ get translation of bank statement from *Slang*
    * ✅ print *Application to transfer money to a Portuguese bank* and sign it
    * ✅ scan bank declaration, translated bank statement and application to one PDF
4. {-&nbsp;Add curriculum vitae&nbsp;-}
    * validate the latest CV
    * add the actual updates
5. {+&nbsp;Statement on absence of tax debts&nbsp;+}
    * ✅ print and sign it
    * ✅ scan
6. {+&nbsp;Registration card in Bishkek&nbsp;+}
    * ✅ scan

### Incubator's declarations

1. {-&nbsp;Incubator's declarations: requires at least one incubator interested in your project&nbsp;-}
    * send a requests to:
        - Centro de Incubação e Desenvolviment - http://www.lispolis.pt/
        - Centro de Incubação Atlântico | Atlântico Business School - Centro de Negócio, Tecnologia e Incubação Empresarial - http://www.abs.pt/
        - Espaço Bazaar - https://www.bazaar.com.pt/
        - Startup Leiria - https://startupleiria.com/
