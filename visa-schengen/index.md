/visa-schengen/# Получение шенгенской визы в Бишкеке

Запись на подачу документов для австрийской визы: https://visa.vfsglobal.com/kgz/ru/aut/book-an-appointment

#### Начальная ситуация

1. ~В Вене находится паспорт с просроченной американской визой~ Паспорт доставлен 12.11.2022, но виза в нем уже просрочена;
2. ~Консульство США в Вене предложило приехать к ним и продлить визу, полагаю они бы не делали такое предложение, если бы не были бы уверены в положительном результате предприятия~ Записан на собеседование на 05.05;

#### Что требуется

1. Для поездки в Вену очевидно нужна австрийская виза;
2. Чтобы получить австрийскую визу, нужно обосновать необходимость поездки, для этого требуется собрать дополнительный пакет документов (помимо [основного пакета](#%D0%B2%D0%B8%D0%B7%D0%B0-%D0%B5%D1%81-%D0%BE%D1%81%D0%BD%D0%BE%D0%B2%D0%BD%D0%BE%D0%B9-%D0%BF%D0%B0%D0%BA%D0%B5%D1%82-%D0%B4%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%BE%D0%B2))

#### Текущая ситуация

1. Сдал документы на шенгенскую визу (Австрийская республика) в Бишкеке 04.04.2023;
2. Цена [11,440.00 Сом ](https://gitlab.com/pheix/escape-to-the-void/-/raw/main/visa-schengen/assets/visa-application-payment-checkout.pdf) (**80EUR** — виза Австрийской республики, **40EUR** — консульский сбор, **1,03EUR** — SMS информирование, см. [Отчет агента](https://gitlab.com/pheix/escape-to-the-void/-/raw/main/visa-schengen/assets/visa-application-invoice.pdf));
3. Анкету пришлось переписывать прямо у окошка — заполненная анкета оказалась устаревшей, в идеале нужно было прийти к ним и попросить распечатать анкету заранее, затем отксерить и заполнить;
4. Документы приняли, однако посчитали пакет неполным:
    * документы на квартиру, свидетельства о браке и рождении детей требуют официального перевода на английский язык (то есть можно было просто на улице в любом бюро переводов сделать) — впервые втретил такое такое требование;
    * спросили про оплату броней самолета — сказал, что бронь не оплачена, врать не стал: в списке документов — билеты на самолет ✅ бронь, 🚫 оплачено, на самом деле это хорошо, так как иначе вообще было бы пусто по билетам;
5. В связи с пунктом №4 пришлось подписать заявление — предоставил неполный пакет документов, претензий не имею;
6. Сразу предупредили, что месяц — минимальный срок рассмотрения и ничего не гарантируем.

После приема документов пришло письмо:

> Dear KONSTANTIN NARKHOV,
>
> Your visa application reference number: BISH/040423/0003/01 has been dispatched to the Nur Sultan Operation Centre, Kazakhstan for processing. An auto e-mail will be generated to inform you when the application is processed and is available for collection at the application Centre or Couriered
>
> Please note this is an auto generated e-mail. Please do NOT reply to this email
>
> Regards, VFS Global

Честно говоря, оцениваю шансы как средние. Обычно я сдавал на шенгенскую визу полный набор, и если требовалось что-то донести, всегда приносил на повторном приеме. Но тут ситуация сложилась тяжелая, во-первых срок переноса записи визы US — октябрь 2023, перенести на ближайшие дни Шенген невозможно — свободных слотов нет. Короче говоря, разумно было бы {-&nbsp;не подавать сегодня документы вообще&nbsp;-} — перенести запись визы US, пойти на консультацию в VFS, возможно оплатить **услугу заполнения и анкеты и верификации документов** (почему я о ней не подумал ранее!!!), собрать полный необходимый пакет, записаться на ближайший свободный слот и быть уверенным, что ты сделал все, что от тебя зависит. Но я рискнул, посмотрим, что из этого выйдет.

При этом в уме держим факт выдачи визы всего на пару недель, как я заказывал к анкете: с 04.05.2023 по 19.05.2023. Тем не менее, это будет все равно успех — нет отказа, возможность получить визу US. Вобщем, скажем честно, если выдадут Шенген день-в-день — это будет справедливо.

В заключении я еще раз убедился, что в этих краях следует внимательно читать и фотографировать агитационные бумажки/документы на стенах. Там много важного и интересного (стоимость, время консультаций и т.п.).

Что забавно:

* ничего не сказали про выписки из банков (Demir — выписка в январе, Bakai — выписка в феврале);
* бронь от AirAstana очень помогла;
* к фото вопросов не было, в МСК каждый раз спрашивали — откуда такие ужасные фото, у Греков заставили перефотографироваться;

Ожидаемо вопросы были только к одному ⚠️-пункту из моега списка, хотя я более всего паниковал из-за банковских выписок. Возможно ситуацию спасла сделанная за день выписка из Альфы. При этом вопрос про переводы документов я, конечно, без посторонней консультации никогда бы не додумал. В сухом остатке, я считаю, что самостоятельная подготовка выполнена очень хорошо. Главный минус: документы поданы поздно, думаю, начало марта было идельным периодом, но что уже сейчас поделать 🥹

### Обновление 11.04.2023

Посольство Австрийской Республики в Астане приняло мое заявление на визу к рассмотрению:

>  Dear KONSTANTIN NARKHOV,
>
> Visa application reference number BISH/040423/0003/01 is under process at Embassy of Austria in Kazakhstan
An auto e-mail will be generated to inform you when the application is processed and is available for collection at the application Centre or Couriered
Please note this is an auto generated e-mail. Please do NOT reply to this email
>
> Regards, VFS Global

### Обновление 14.04.2023

Отказ: https://gitlab.com/pheix/escape-to-the-void/-/raw/main/visa-schengen/assets/visa-at-refusal.pdf

## Въезд в Австрию

[Актуальная информация](https://www.bmeia.gv.at/ru/oeb-moskau/reisen-nach-oesterreich/coronavirus-covid-19-und-reisen/) по COVID-19 c 16.05.2022 (в соотв. с BGBl. 186/2022):

* При въезде из Российской Федерации в Австрию больше не требуется подтверждение «3G» (привит ИЛИ переболел ИЛИ пройден ПЦР-тест);
* Въезд в Австрию возможен без ограничений по COVID-19.

При этом в австрийском визовом центре в Бишкеке мне дали [документ](https://gitlab.com/pheix/escape-to-the-void/-/raw/main/visa-schengen/assets/austria-entrance-details.jpg), в котором есть [ссылка](https://www.oesterreich.gv.at/en/themen/coronavirus_in_oesterreich/pre-travel-clearance.html) на форму предварительной регистрации. По ссылке в настоящий момент ограничений на въезд нет. Однако для уверенности нужно будет иметь на руках ПЦР-тест, [сделанный](https://dgsen.kg/uslugi/pcr-analiz-na-covid-19-teper-mozhno-sdat-vsego-za-490-somov.html) не менее чем за 72 часа до въезда в Австрию.

Подача на шенгенскую визу за 6 месяцев до даты планируемой поездки: https://visa.vfsglobal.com/kaz/ru/aut/faq (внимание — референс на сайт VFS 🇰🇿)

Непосредственно перед выездом нужно сдеать Covid-19 ПЦР тест.

## Виза ЕС — основной пакет документов

1. ✍️ ~Заполненная анкета на получение визы ЕС, на бланке Австрии (на оба паспорта)~, бланк: https://www.bmeia.gv.at/fileadmin/user_upload/Allgemein/Formulare/Antragsformular_Visum_C_NEU.pdf;
2. ~Справка о наличие банковского счета из Bakai;~
3. 🖨️ ~Фотографии~;
4. ~Выписка с банковского счета~ — ⚠️ {-&nbsp;просроченные Bakai и Demir&nbsp;-} и актуальная из Альфы;
5. 🖨️ ~Распечатанный контракт~;
6. ~Новая регистрация в Бишкеке~;
7. ~Страховка~ — рекомендовали сделать в местной фирме (д.б. указан COVID-19), я выбрал [Страховую компанию "АЮ Гарант"](http://agi.kg/), ~[Aльфа-Страхование online](https://www.alfastrah.ru/individuals/travel/)~;
8. 🖨️ ~Брони билетов~ — ⚠️ {-&nbsp;бронь с AirAstana&nbsp;-} (Бишкек↔Астана, Астана↔Вена);
9. 🖨️ ~Брони отелей на даты нахождения в Австрии~;
10. 🖨️ ~Мотивационное (сопроводительное) письмо с описанием и целью поездки (на основание письма в консульство США в Бишкеке)~;
11. Ксерокопии и распечатки, которые прилагаются к пакету документов на визу:
    * ~заграничный паспорт~;
    * ~паспорт РФ~;
    * ~свидетельство о браке (обновленное свидетельство)~;
    * ~свидетельства о рождении детей (обновленное свидетельство Макара)~;
    * ~свидетельство на собственность~ и ~выписка из Единого государственного реестра недвижимости об основных характеристиках и зарегистрированных правах на объект недвижимости~;
    * ~СТС~;
    * ~регистрационные талоны детей в Бишкеке~;
    * ~переписка с консульством США в Вене по продлению визы (указана в мотивационном письме)~.

Я сделал тестовую запись в визовый центр на 14.10.2022, 11:00. Основные вопросы для уточнения:

> * сделают ли визу для нерезидента;
> * стоит ли указывать мультивизу или сейчас для россиян доступны только одноразовые;
> * согласовать пакет документов (основной и дополнительный пакеты);
> * страховка российской страховой компании подойдёт ли;
> * есть ли ограничения на въезд, связанные с Covid-19.

Визы для нерезидентов делают. Указать именно мультивизу, сделать страховку в местной фирме. Ограничений, связанных с Covid-19, сейчас нет (см. выше). Пакет документов, который я планирую передать, подойдёт.

## Виза ЕС — дополнительный пакет документов

Все, что может подтвердить собеседование в консульстве США в Вене:

1. ~Заявление по форме DS-160;~
2. ~Подтверждение оплаты консульского сбора;~
3. ~Подтверждение записи на собеседование.~

## Виза US

В связи с отказом в выдаче шенгенской визы я **{+&nbsp;перезаписался на 27.10.2023&nbsp;+}**, не знаю смогу ли я въехать в австрию в октябре, но мало ли.

1. ~Заявление по форме [DS-160](https://ceac.state.gov/genniv/);~
2. ~Оплата сбора за обращение в размере $160, доступно банковским переводом или дебетовой VISA/MC;~
3. ~Получение payment receipt number;~
4. ~Запись на собеседование в Вене по номеру платежа: [https://cgifederal.secure.force.com/ApplicantHome](https://cgifederal.secure.force.com/ApplicantHome)~;
5. ~Подтверждение записи на собеседование~.

Документы об оплате визового сбора, анкету DS-160 и подтверждение записи на собеседование следует собрать в **Дополнительный пакет документов** для получения визы ЕС.
